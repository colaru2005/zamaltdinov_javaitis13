package ru.itis.directExchange.publisher;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.springframework.amqp.core.ExchangeTypes;

import java.nio.charset.StandardCharsets;

public class Task {

    static void createTask(int timeToSleep, String routingKey) throws Exception {
        int counter = 0;

        do {
            Thread.sleep(timeToSleep);
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            try (Connection connection = factory.newConnection();
                 Channel channel = connection.createChannel()) {
                channel.exchangeDeclare("direct_logs", ExchangeTypes.DIRECT);
                String message = "Message from publisher N " + counter;
                channel.basicPublish("", routingKey, null, message.getBytes(StandardCharsets.UTF_8));
                System.out.println("Message type [" + routingKey + "] is sent into Exchange " + counter++);
            }
        } while (true);
    }
}

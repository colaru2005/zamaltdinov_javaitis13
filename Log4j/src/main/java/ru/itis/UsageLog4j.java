package ru.itis;

import java.util.logging.Level;
import java.util.logging.Logger;

public class UsageLog4j {

    private static final Logger logger = Logger.getLogger(UsageLog4j.class.getName());

    public static void main(String[] args) {
        logger.info("This is level info logging");
        logger.log(Level.WARNING, "This is level warning logging");
        logger.log(Level.SEVERE, "This level severe logging");
    }
}

package ru.itis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenerationOfCarNumbersApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenerationOfCarNumbersApplication.class, args);
    }

}

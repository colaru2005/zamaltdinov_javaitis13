package example4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamExample {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Hello");
        list.add("How are you?");
        list.add("Ok");
        list.add("GoodBye!");

        List<Integer> changeList = list.stream().map(element ->element.length())
                .collect(Collectors.toList());

        System.out.println(changeList);

        int[] array = {5, 9, 3, 8, 1};
        array = Arrays.stream(array).map(i -> {
            if (i % 3 == 0) {
                i = i / 3;
            }
            return i;
        }).toArray();

        System.out.println(Arrays.toString(array));
    }
}

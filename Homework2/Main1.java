import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main1 {

    private static String letter[] = {"А", "Е", "Т", "О", "Р", "Н", "У", "К", "Х", "С", "В", "М"};
    private static char letterOther[] ={'В'};
    private static final String extraNumber = " 116 RUS";
    private static List<String> numbersCar = new ArrayList<>();

    public static void main(String[] args) {
        int count = 0;
        while (count < 5) {
            String str1 = getRandomNumber();
            System.out.println(str1);
            count++;
        }

    }


    public static String getRandomNumber() {
        return generateRandomNumber();
    }


    public String getNextNumber() {
        return generateNextNumber();
    }

    public static String generateRandomNumber() {
        Random random = new Random();
        int firstChar = random.nextInt(letter.length);
        int secondChar = random.nextInt(letter.length);
        int thirdChar = random.nextInt(letter.length);
        int firstNumber = random.nextInt(10);
        int secondNumber = random.nextInt(10);
        int thirdNumber = random.nextInt(10);
        String str = letter[random.nextInt(letter.length)];

        String numberOfCar = letter[firstChar] + firstNumber +
                secondNumber + thirdNumber +
                letter[secondChar] + letter[thirdChar] + extraNumber;
        boolean check = isCheck(numberOfCar);
        if (!check) {
            numbersCar.add(numberOfCar);
        } else {
            throw new IllegalArgumentException("Such number exists");
        }
        return numberOfCar;
    }

    private static boolean isCheck(String numberOfCar) {
        for (String number: numbersCar) {
            if (numberOfCar.equals(number)) {
                return true;
            }
        }
        return false;
    }

    public static String generateNextNumber() {
        String lastNumberFromStore = numbersCar.get(numbersCar.size() - 1);
        String carNumber = numbersCar.get(numbersCar.size() - 1);
        String str = carNumber.substring(1,4);
        int parseNumber = Integer.parseInt(str);

        if (parseNumber == 999) {
            String old = lastNumberFromStore.substring(4,6);
            for (int i = 0; i < letterOther.length; i++) {
                if (old.charAt(old.length() -1) < letterOther[i]) {
                    char x = old.charAt(letterOther.length - 1);
                    char y = letterOther[i];
                    x = y;

                }

            }
        }
        parseNumber++;


        String str1 = lastNumberFromStore.substring(0,1);
        String str2 = lastNumberFromStore.substring(4,14);

        if (parseNumber < 100) {
            return str1 + "0" + parseNumber + str2;
        }

        return str1 + parseNumber + str2;
    }
}

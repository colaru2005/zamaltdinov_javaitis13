package ru.itis.factoryMethod;

import java.time.LocalDateTime;

public class Act implements Document {

    private LocalDateTime dateTime;
    private String from;

    public Act(LocalDateTime dateTime, String from) {
        this.dateTime = dateTime;
        this.from = from;
    }

    @Override
    public String getType() {
        return "ACT";
    }

    @Override
    public String getFrom() {
        return from;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    @Override
    public String toString() {
        return getType() + "[" + getFrom() + ", " + getDateTime() + "]";
    }
}

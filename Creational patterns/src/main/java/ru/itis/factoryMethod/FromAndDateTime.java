package ru.itis.factoryMethod;

import java.time.LocalDateTime;

public class FromAndDateTime implements Information {

    private String from;
    private LocalDateTime dateTime;


    public FromAndDateTime(String from, LocalDateTime dateTime) {
        this.from = from;
        this.dateTime = dateTime;
    }

    public String getFrom() {
        return from;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }
}

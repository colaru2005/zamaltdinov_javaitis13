package ru.itis.factoryMethod;

public class StatementsProducer implements DocumentsProducer {

    @Override
    public Document create(Information information) {
        FromAndDateTime fromAndDateTime = (FromAndDateTime)information;
        return new Statement(fromAndDateTime.getDateTime(), fromAndDateTime.getFrom());
    }
}

package ru.itis.abstractFactory;

public interface Statement {
    String getDescription();
}

package ru.itis.abstractFactory;

import java.util.UUID;

public class DocumentProducerWithIdImpl implements DocumentProducer {

    private static class ActImpl implements Act {
        private String id;

        public ActImpl(String id) {
            this.id = id;
        }

        @Override
        public String getDescription() {
            return "Act[id = " + id + " ]";
        }
    }

    private static class StatementImpl implements Statement {
        private String id;

        public StatementImpl(String id) {
            this.id = id;
        }

        @Override
        public String getDescription() {
            return "Statement[id = " + id + " ]";
        }
    }

    @Override
    public Act createAct() {
        return new ActImpl(UUID.randomUUID().toString());
    }

    @Override
    public Statement createStatement() {
        return new StatementImpl(UUID.randomUUID().toString());
    }
}

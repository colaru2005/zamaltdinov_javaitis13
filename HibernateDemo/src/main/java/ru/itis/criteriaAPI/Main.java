package ru.itis.criteriaAPI;

import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.itis.entity.User;
import ru.itis.util.HibernateUtil;

import javax.persistence.criteria.*;

@Log4j2
public class Main {
    public static void main(String[] args) {

        log.info("Hibernate tutorial started");
        //Получаем готовый SessionFactory и сразу создаем сессию
        Session session = HibernateUtil.getSessionFactory().openSession();
//        session.getTransaction().begin();

//        log.info("Transaction start");
//
//        User user = new User();
//        user.setEmail("newfromapp3@email.com");
//        user.setUsername("newfromapp3");
//        user.setUserpassword("sfdsdfsdf12sdf");
//
//        session.save(user);
//        session.getTransaction().commit();

        //подготовка запроса - получение всех пользователей
//        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
//        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
//        Root<User> root = criteriaQuery.from(User.class);
//        criteriaQuery.select(root); // конечный запрос select из таблицы User
//
//        // выводит всех user id больше 10000
//        criteriaQuery.select(root).where(criteriaBuilder.gt(root.get("id"), 10000));
//
//        //выполнение запроса с получением результата
//        Query query = session.createQuery(criteriaQuery);
//        List<User> results = query.getResultList();

        //удаление пользователя по id
//        CriteriaBuilder cb = session.getCriteriaBuilder();
//        CriteriaDelete<User> criteriaDelete = cb.createCriteriaDelete(User.class);
//        Root<User> root = criteriaDelete.from(User.class);
//        criteriaDelete.where(cb.equal(root.get("id"), 10154));
//        session.createQuery(criteriaDelete).executeUpdate();
//        session.getTransaction().commit();

        //обновление
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaUpdate<User> criteriaUpdate = cb.createCriteriaUpdate(User.class);
        Root<User> root = criteriaUpdate.from(User.class);

        criteriaUpdate.set("email", "testwere@email.com");
        criteriaUpdate.where(cb.equal(root.get("id"),10515));

        Transaction transaction = session.beginTransaction();
        session.createQuery(criteriaUpdate).executeUpdate();
        transaction.commit();

        session.close();
        HibernateUtil.close();

    }
}

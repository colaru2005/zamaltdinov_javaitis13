package ru.itis.HQL;

import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import ru.itis.entity.User;
import ru.itis.util.HibernateUtil;

import java.util.List;

@Log4j2
public class Main {
    public static void main(String[] args) {
       log.info("Hibernate tutorial started");

       Session session = HibernateUtil.getSessionFactory().openSession();

       //получение всех пользователей
//       Query query = session.createQuery("from User");
//       List<User> users = query.getResultList();

        //получение пользователей по оператору like
//        Query query = session.createQuery("from User u where u.email like :text");
//        query.setParameter("text", "%a%");
//        List<User> users = query.getResultList();

        //постраничный вывод. С первой строки вывести 10 строк
//        Query query = session.createQuery("from User");
//        query.setFirstResult(1);
//        query.setMaxResults(10);
//        List<User> users = query.getResultList();
//        log.info(users.size());

        //получение одного объекта
//        Query<User> query = session.createQuery("from User where id = :id");
//        query.setParameter("id", 10025L);
//        User user = query.uniqueResult();
//        log.info(user);

        //выполнение агрегированных операций
//        Query<Long> query = session.createQuery("select count(u.id) from User u where email like :text");
//        query.setParameter("text", "%email%");
//        Long count = query.uniqueResult();
//        log.info(count);

        String query = "select * from todolist.user_data";
        NativeQuery sqlQuery = session.createSQLQuery(query);
        sqlQuery.setMaxResults(10);
        sqlQuery.addEntity(User.class);
        List<User> list = sqlQuery.list();

        log.info(list);

       session.close();
       HibernateUtil.close();
    }
}

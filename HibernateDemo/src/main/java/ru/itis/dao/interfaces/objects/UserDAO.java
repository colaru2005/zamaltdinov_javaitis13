package ru.itis.dao.interfaces.objects;

import ru.itis.dao.interfaces.CommonDAO;
import ru.itis.dao.interfaces.FindDAO;
import ru.itis.entity.User;

public interface UserDAO extends CommonDAO<User>, FindDAO<User> {

    User getByEmail(String email);
}

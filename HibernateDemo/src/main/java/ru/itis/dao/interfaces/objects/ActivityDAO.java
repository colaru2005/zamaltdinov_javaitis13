package ru.itis.dao.interfaces.objects;

import ru.itis.dao.interfaces.CommonDAO;
import ru.itis.entity.Activity;
import ru.itis.entity.User;

public interface ActivityDAO extends CommonDAO<Activity> {

    Activity getByUser(String email);
    Activity getByUser(User user);
}

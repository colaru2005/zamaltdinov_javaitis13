package ru.itis.dao.impl;

import org.hibernate.Session;
import org.hibernate.query.Query;
import ru.itis.dao.interfaces.objects.ActivityDAO;
import ru.itis.entity.Activity;
import ru.itis.entity.User;
import ru.itis.util.HibernateUtil;

public class ActivityDAOImpl implements ActivityDAO {

    @Override
    public Activity get(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Activity priority = session.get(Activity.class, id);
        session.close();
        return priority;
    }

    @Override
    public void update(Activity obj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(obj);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void delete(Long id) {
        throw new IllegalArgumentException("You can't delete activity by yourself");
    }

    @Override
    public void add(Activity obj) {
        throw new IllegalArgumentException("You can't add activity by yourself");
    }

    @Override
    public Activity getByUser(String email) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query<Activity> query = session.createQuery("FROM Activity where user.email like :email");
        query.setParameter("email", "%" + email + "%");
        Activity stat = query.uniqueResult();
        session.close();
        return stat;
    }

    @Override
    public Activity getByUser(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query<Activity> query = session.createQuery("FROM Activity where user.email like :email");
        query.setParameter("email", "%" + user.getEmail() + "%");
        Activity activity = query.uniqueResult();
        session.close();
        return activity;
    }
}

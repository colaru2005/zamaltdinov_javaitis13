package ru.itis.dao.impl;

import org.hibernate.Session;
import org.hibernate.query.Query;
import ru.itis.dao.interfaces.objects.PriorityDAO;
import ru.itis.entity.Priority;
import ru.itis.util.HibernateUtil;

import java.util.List;

public class PriorityDAOImpl implements PriorityDAO {

    @Override
    public Priority get(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Priority priority = session.get(Priority.class, id);
        session.close();
        return priority;
    }

    @Override
    public void update(Priority obj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(obj);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void delete(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Priority priority = new Priority();
        priority.setId(id);
        session.delete(priority);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void add(Priority obj) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(obj);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public List<Priority> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query<Priority> query = session.createQuery("FROM Priority");
        List<Priority> list = query.getResultList();
        session.close();
        return list;
    }

    @Override
    public List<Priority> findAll(String email) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query<Priority> query = session.createQuery("FROM Priority p WHERE p.user.email=:email");
        query.setParameter("email", "%" + email + "%");
        List<Priority> list = query.getResultList();
        session.close();
        return list;
    }
}

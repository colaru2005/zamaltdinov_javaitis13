package ru.itis.dao;

import org.junit.Before;
import org.junit.Test;
import ru.itis.model.User;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class UserDaoTest {

    private UserDao userDao;

    @Before
    public void setUp() throws Exception {
        this.userDao = new UserDaoImpl();
    }

    @Test
    public void getUserByUsername_should_return_true() throws Exception{
        User userFromStorage = userDao.getUserByUsername("rasim@gmail.com");
        assertThat(userFromStorage).isNotNull();
        assertThat(userFromStorage.getUsername()).isEqualTo("rasim@gmail.com");
    }

    @Test
    public void getUserByUsername_null_user()throws Exception {
        User userFromStorage = userDao.getUserByUsername("alex@gmail.com");
        assertThat(userFromStorage).isNull();
    }

    @Test
    public void findAllUsers_contain() throws Exception {
        List<User> users = userDao.findAllUsers();
        assertThat(users.get(2)).isEqualToComparingFieldByField(
                new User("ivan@gmail.com", "ADMIN")
        );
        assertThat(users).contains(new User("ivan@gmail.com", "ADMIN"));

    }
}
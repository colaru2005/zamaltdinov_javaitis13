package ru.itis.example3;

public class Main {
    public static void main(String[] args) {
        Student student = new Student();
        StringBuilder sb = new StringBuilder("Ivan");
        student.setName(sb);
        student.setCourse(2);
        student.setGrade(7);
        student.showInfo();
    }
}

package ru.itis.lambda_expressions;

public interface StudentCheck {
    boolean check(Student s);
}

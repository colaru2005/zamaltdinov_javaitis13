package ru.itis.lambda_expressions;

public class CheckOverGrade implements StudentCheck{
    @Override
    public boolean check(Student s) {
        return s.getAvgGrade() > 8;
    }
}

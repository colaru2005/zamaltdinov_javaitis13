package ru.itis.example1;

public class Main {
    public static void main(String[] args) {
        Sum sum = new Sum();
        sum.sum();
        sum.sum(3);
        sum.sum(3,5);
        sum.sum(3,5,7);
        sum.sum(3,5,7,11);
    }
}

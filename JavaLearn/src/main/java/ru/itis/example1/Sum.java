package ru.itis.example1;

public class Sum {

    public int sum() {
        int result = 0;
        System.out.println("Sum all numbers: " + result);
        return result;
    }

    public int sum(int a) {
        int result1 = a;
        System.out.println("Sum all numbers: " + result1);
        return result1;
    }

    public int sum(int b, int c) {
        int result2 = b + c;
        System.out.println("Sum all numbers: " + result2);
        return result2;
    }

    public int sum(int d, int e, int f) {
        int result3 = d + e + f;
        System.out.println("Sum all numbers: " + result3);
        return result3;
    }

    public int sum(int g, int h, int i, int j) {
        int result4 = g + h + i + j;
        System.out.println("Sum all numbers: " + result4);
        return result4;
    }
}

package ru.itis.example4;

public class Cat extends Pet {

    public Cat (String name) {
        this.setName(name);
        System.out.println("I am cat, my name is: " + name);
    }

    public void sleep() {
        System.out.println("Cat sleeps");
    }
}

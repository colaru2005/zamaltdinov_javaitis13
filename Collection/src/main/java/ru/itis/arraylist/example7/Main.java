package ru.itis.arraylist.example7;

import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> items = WorkOnArrayList.getList();
        System.out.println(items);
        Collections.shuffle(items);
        System.out.println(items);
    }
}

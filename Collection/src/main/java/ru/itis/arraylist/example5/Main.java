package ru.itis.arraylist.example5;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> item = WorkOnArrayList.getList();
        if (item.contains("orange")) {
            System.out.println("It's true");
        } else {
            System.out.println("It's wrong");
        }
    }
}

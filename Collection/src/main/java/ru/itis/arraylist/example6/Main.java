package ru.itis.arraylist.example6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> items = WorkOnArrayList.getList();
        System.out.println(items);
        List<String> itemsCopy = new ArrayList<>();
        itemsCopy.add("A");
        itemsCopy.add("B");
        itemsCopy.add("C");
        itemsCopy.add("D");
        itemsCopy.add("E");
        itemsCopy.add("F");
        System.out.println(itemsCopy);
        System.out.println("_______________________________");
        Collections.copy(itemsCopy, items);
        System.out.println(items);
        System.out.println(itemsCopy);
    }
}

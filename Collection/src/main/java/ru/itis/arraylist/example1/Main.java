package ru.itis.arraylist.example1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String[] array = new String[]{"apple", "pineapple", "banana", "cherry", "grape", "orange"};
        List items = addItemList(array);
        Collections.sort(items, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.toString().compareTo(o2.toString());
            }
        });
        System.out.println(items);

    }

    static List addItemList(String[] items) {
        List<String> listItems = new ArrayList<>();
        for (int i = 0; i < items.length; i++) {
            listItems.add(items[i]);
        }
        return listItems;
    }
}

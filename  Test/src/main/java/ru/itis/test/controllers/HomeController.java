package ru.itis.test.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.test.forms.SignUpForm;
import ru.itis.test.service.SignUpService;

@Controller
public class HomeController {

    @Autowired
    private SignUpService signUpService;

    @GetMapping("/")
    public String home() {
        return "greeting";
    }

    @GetMapping("/signIn")
    public String SignInPage() {
        return "signIn";
    }


    @GetMapping("/signUp")
    public String getSignUpPage() {
        return "signUp";
    }

    @PostMapping("/signUp")
    public String signUpUser(@ModelAttribute("signUp") SignUpForm form) {
        signUpService.signUp(form);
        return "redirect:/signIn";
    }

}

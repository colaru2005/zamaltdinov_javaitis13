package ru.itis.test;

public class Man {

    final String pol;
    private String name;
    private int age;
    private int weight;

    public String getName(){
        return name;
    }

    public void setName(String s) {
        name = s;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int i) {
        if (i > 0) {
            age = i;
        }
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int w) {
        if (w > 0) {
            weight = w;
        }
    }

    Man(String pol) {
        this.pol = pol;
    }


}

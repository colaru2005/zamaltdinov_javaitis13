package ru.itis.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.MenuOrder;

import java.util.logging.LogManager;
import java.util.logging.Logger;

@Service
public class RabbitMQSender {

    private static final Logger logger = LogManager.getLogManager().getLogger(RabbitMQSender.class.toString());

    private AmqpTemplate rabbitTemplate;
    private Queue queue;


    public RabbitMQSender(AmqpTemplate rabbitTemplate, Queue queue) {
        this.rabbitTemplate = rabbitTemplate;
        this.queue = queue;
    }

    public void send(MenuOrder menuOrder) {
        rabbitTemplate.convertAndSend(queue.getName(), menuOrder);
        logger.info("Sending Message to the Queue : " + menuOrder.toString());
    }
}

package deadLock;

public class Thread20 extends Thread {


    public void run() {
        System.out.println("Thread20: Попытка захватить монитор объекта lock2");
        synchronized (DeadLockEx.lock1) {
            System.out.println("Thread20: Монитор объекта lock2 захвачен");
            System.out.println("Thread20: Попытка захватить монитор объекта lock1");
            synchronized (DeadLockEx.lock2) {
                System.out.println("Мониторы объектов lock1 и lock2 захвачены");

            }
        }
    }
}

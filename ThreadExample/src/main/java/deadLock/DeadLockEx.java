package deadLock;

public class DeadLockEx {

    public static final Object lock1 = new Object();
    public static final Object lock2 = new Object();
}

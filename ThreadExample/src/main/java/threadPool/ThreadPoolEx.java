package threadPool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolEx {
    public static void main(String[] args) {
        // Интерфейс для создания threadPool
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 10; i++) {
            executorService.execute(new RunnableImpl());
        }
        // закрытия TreadPool
        executorService.shutdown();
        System.out.println("Main ends");
    }
}

package semaphore;

import java.util.concurrent.Semaphore;

public class SemaphoreEx {
    public static void main(String[] args) {
        Semaphore callBox = new Semaphore(2);

        new Person("Rasim", callBox);
        new Person("Oleg", callBox);
        new Person("Ivan", callBox);
        new Person("Viktor", callBox);
        new Person("Marina", callBox);
    }
}

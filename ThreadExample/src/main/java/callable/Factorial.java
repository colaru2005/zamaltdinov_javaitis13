package callable;

import java.util.concurrent.Callable;

public class Factorial implements Callable<Integer> {

    private int f;

    public Factorial(int f) {
        this.f = f;
    }

    @Override
    public Integer call() throws Exception {
        if (f <= 0) {
            throw new Exception("You entered wrong number ");
        }
        int result = 1;
        for (int i = 1; i <=  f; i++) {
            result *= i;
        }

        return result;
    }
}

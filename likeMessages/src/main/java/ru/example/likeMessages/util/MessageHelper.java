package ru.example.likeMessages.util;

import ru.example.likeMessages.models.User;

public abstract class MessageHelper {

    public static String getAuthorName(User author) {
        return author != null ? author.getUsername() : "<none>";
    }
}

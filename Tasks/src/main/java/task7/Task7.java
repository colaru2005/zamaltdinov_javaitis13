package task7;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        int tempNumber, number;
        boolean numberIsPrime = true;
        Scanner scanner = new Scanner(System.in);
        number = scanner.nextInt();
        scanner.close();
        for (int i = 2; i <= number / 2; i++) {
            tempNumber = number % i;

            if (tempNumber == 0) {
                numberIsPrime = false;
                break;
            }
        }

        if (numberIsPrime) {
            System.out.println(number + " число является простым");
        } else {
            System.out.println(number + "число не является простым");
        }
    }
}

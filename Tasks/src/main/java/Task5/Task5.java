package Task5;

import java.util.HashMap;
import java.util.Map;

public class Task5 {
    public static void main(String[] args) {
        String st = "I often forget my door door keys";
        String[] words = st.split(" ");

        Map<String, Integer> keyValue = new HashMap<String, Integer>();

        for (int i = 0; i <= words.length - 1; i++) {

            if (keyValue.containsKey(words[i])) {

                int counter = keyValue.get(words[i]);

                keyValue.put(words[i], counter + 1);

            } else {
                keyValue.put(words[i], 1);
            }
        }
        System.out.println(keyValue);
    }
}

package task19;

public class Task19 {
    public static void main(String[] args) {
        String st1 = "Мы готовимся   к интервью   на вакансию   Java разработчика";

        char[] chars = st1.toCharArray();

        StringBuffer stringBuffer = new StringBuffer();

        for (int i = 0; i < chars.length; i++) {

            if ((chars[i] != ' ') && (chars[i] !='\t')) {
                stringBuffer.append(chars[i]);
            }

        }
        System.out.println(stringBuffer);
    }
}
